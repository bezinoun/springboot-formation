package com.maltem.springboot.swagger.controller;

import java.util.List;

import org.jfairy.producer.person.Person.Sex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.maltem.springboot.swagger.model.Student;
import com.maltem.springboot.swagger.service.StudentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 
 */

@RestController
@RequestMapping(value = "/api/students")
@Api(value = "API to search Student from a Student Repository by different search parameters", description = "This API provides the capability to search Student from a Student Repository", produces = "application/json")
public class StudentController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private StudentService studentService;

	@ApiOperation(value = "Get All Student", produces = "application/json")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	 @PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<Object> getAllStudents() {
		logger.debug("Getting All students ......");
		List<Student> student = null;
		try {
			student = studentService.getAll();
			logger.debug("Getting All students ...... ::");
		} catch (Exception ex) {
			logger.error("Error occurred in searchStudentById >>", ex, ex.getMessage());
			return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Object>(student, HttpStatus.OK);
	}

 
 	@ApiOperation(value = "Search for all Students whose age is greater than input age", produces = "application/json")
 	@RequestMapping(value = "/greaterThanAge/{age}", method = RequestMethod.GET)
 	public ResponseEntity<Object> filterStudentsByAge(@ApiParam(name = "age", value = "filtering age", example = "1",
 
 			required = true) @PathVariable Integer age) {
 		List<Student> studentList = null;
 		try {
 			studentList = studentService.filterByAge(age);
 		} catch (Exception ex) {
 			logger.error("Error occurred in filterStudentsByAge >>", ex, ex.getMessage());
 			return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
 		}
 		return new ResponseEntity<Object>(studentList, HttpStatus.OK);
 	}
 
 	
 	@RequestMapping(value = "/{age}", method = RequestMethod.GET)
 	
 	public ResponseEntity<Object> filterStudentsByAgePage(@PathVariable Integer age , Pageable page) {
 		Page<Student> studentList = null;
 		try {
 			studentList = studentService.findByAgePage(age , page);
 		} catch (Exception ex) {
 			logger.error("Error occurred in filterStudentsByAge >>", ex, ex.getMessage());
 			return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
 		}
 		return new ResponseEntity<Object>(studentList, HttpStatus.OK);
 	}
 	
 	

 	
 	@RequestMapping(value = "/fromSex/{sex}", method = RequestMethod.GET)
 	public ResponseEntity<Object> filterStudentsBySex(
 			@PathVariable Sex sex) {
 		List<Student> studentList = null;
 		try {
 			studentList = studentService.filterBySex(sex);
 		} catch (Exception ex) {
 			logger.error("Error occurred in filterStudentsBySex >>", ex, ex.getMessage());
 			return new ResponseEntity<Object>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
 		}
 		return new ResponseEntity<Object>(studentList, HttpStatus.OK);
 	}
 	
 
}
