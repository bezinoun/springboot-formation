package com.maltem.springboot.swagger.service.exception;

public class NoStudentExistsException  extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    public NoStudentExistsException(final String message) {
        super(message);
    }


}
