package com.maltem.springboot.swagger.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Type;
import org.jfairy.producer.person.Person;
import org.joda.time.DateTime;

/**
 
 */
@Entity
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "student_id")
	private Long studentId;

	@Column(name = "first_name", nullable = false, length = 50)
	private String firstName;

	@Column(name = "middle_name", length = 20)
	private String middleName;

	@Column(name = "last_name", length = 20)
	private String lastName;

	@Column
	private String email;
	
	@Enumerated(EnumType.STRING)
	private Person.Sex sex;
	@Column
	private String telephoneNumber;
 
	@Column
	private Date dateOfBirth;
	
	private Integer age;
	@Column
	private String companyEmail;
	@Column
	private String nationalIdentityCardNumber;
	@Column
	private String nationalIdentificationNumber;

//	
//    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="student")
//    private List<Item> items;

	public Student(int studentId, Person p) {
		this.studentId = Long.valueOf(studentId);
		this.nationalIdentityCardNumber = p.nationalIdentificationNumber();
		this.firstName = p.firstName();
		this.middleName = p.middleName();
		this.lastName = p.lastName();
		this.email = p.email();
		this.sex = p.sex();
		this.telephoneNumber = p.telephoneNumber();
		this.dateOfBirth = p.dateOfBirth().toDate();
		this.age = 25;
		this.nationalIdentificationNumber = p.nationalIdentificationNumber();
		this.companyEmail = p.companyEmail();
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Person.Sex getSex() {
		return sex;
	}

	public void setSex(Person.Sex sex) {
		this.sex = sex;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getNationalIdentityCardNumber() {
		return nationalIdentityCardNumber;
	}

	public void setNationalIdentityCardNumber(String nationalIdentityCardNumber) {
		this.nationalIdentityCardNumber = nationalIdentityCardNumber;
	}

	public String getNationalIdentificationNumber() {
		return nationalIdentificationNumber;
	}

	public void setNationalIdentificationNumber(String nationalIdentificationNumber) {
		this.nationalIdentificationNumber = nationalIdentificationNumber;
	}

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

}
