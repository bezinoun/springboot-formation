package com.maltem.springboot.swagger.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.maltem.springboot.swagger.model.Student;
import com.maltem.springboot.swagger.service.StudentService;

@RestController
@RequestMapping("/api/student")
public class StudentCrudController {
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);
    
    @Autowired
    private StudentService studentService;
    

    @RequestMapping(value="", method=RequestMethod.POST)
    public Student createStudent(@RequestBody Student student) {
        LOGGER.debug("Received request to create the {}", student);
        return studentService.save(student);
    }
    
   

    @RequestMapping(value="/{studentId}", method=RequestMethod.GET)
    public Student getStudent(@PathVariable long studentId) {
        return studentService.findOne(studentId);
    }
    
    // GET: http://localhost:8080/students/search?firstname=be
    @RequestMapping(value="/search", method=RequestMethod.GET)
    public List<Student> findStudents(@RequestParam String firstname) {
    	Assert.isTrue(!firstname.isEmpty(), "firstname parameter must be present");
        return studentService.findByFirstnameStartingWith(firstname);
    }
    
  
    @RequestMapping(value="/{studentId}", method=RequestMethod.PUT)
    public Student updateStudent(@PathVariable Long studentId, @RequestBody Student student) {
        student.setStudentId(studentId);
        return studentService.update(student);
    }
    
    // DELETE: http://localhost:8080/students/1
    @RequestMapping(value="/{studentId}", method=RequestMethod.DELETE)
    public String deleteStudent(@PathVariable long studentId) {
        studentService.delete(studentId);
        return studentId + " is Deleted";
    }
    
}