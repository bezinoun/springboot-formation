package com.maltem.springboot.swagger.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.maltem.springboot.swagger.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

	// No need to put query annotation if method name follows naming rule.
	// For details, check Spring Data JPA documentation
	List<Student> findByLastName(String lastname);
	
	Page<Student> findByAge(Integer age, Pageable page);

	@Query("SELECT s FROM Student s WHERE UPPER(s.firstName) LIKE ?1%") // case insensitive
	List<Student> findByFirstNameStartingWithIgnoreCase(String firstname, Sort sort);

	@Query("SELECT COUNT(s) FROM Student s WHERE s.firstName = ?1")
	Integer countByFirstName(String firstname);

}