package com.maltem.springboot.swagger.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.jfairy.Fairy;
import org.jfairy.producer.person.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maltem.springboot.swagger.model.Student;
import com.maltem.springboot.swagger.repository.StudentRepository;
import com.maltem.springboot.swagger.service.exception.NoStudentExistsException;
import com.maltem.springboot.swagger.service.exception.StudentAlreadyExistsException;
import com.sun.istack.NotNull;

/**
 
 */
@Service
public class StudentService {
//	private static Map<Integer, Student> studentDB;

	@Autowired
	private StudentRepository studentRepository;

	private Fairy fairy = Fairy.create();

	@PostConstruct
	public void init() throws Exception {
		for (int i = 0; i < 50; i++) {
			Student student = new Student(i, fairy.person());			
			studentRepository.save(student);
		
		}
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(StudentService.class);

	public List<Student> getAll() {
		List<Student> studentList = studentRepository.findAll();
		return studentList;
	}


	public List<Student> filterByAge(Integer age) {
		List<Student> studentList =  studentRepository.findAll().stream().filter(e -> e.getAge() > age)
				.collect(Collectors.toList());
		
		
		
		return studentList;
	}

	public List<Student> filterBySex(Person.Sex sex) {
		List<Student> studentList = studentRepository.findAll().stream().filter(e -> e.getSex().equals(sex))
				.collect(Collectors.toList());
		return studentList;
	}

	@Transactional
	public Student save(@NotNull final Student student) {
		LOGGER.debug("Creating {}", student);
		if (studentRepository.existsById(student.getStudentId())) {
			throw new StudentAlreadyExistsException(
					String.format("There already exists a student with id=%s", student.getStudentId()));
		}
		return studentRepository.save(student);
	}

	@Transactional(readOnly = true)
	public List<Student> findAll() {
		LOGGER.debug("Retrieving the list of all students");
		return (List<Student>) studentRepository.findAll();
	}

	@Transactional(readOnly = true)
	public Student findOne(Long id) {
		LOGGER.debug("Retrieving a student by student id={}", id);
		Optional<Student> existing = studentRepository.findById(id);
		
		if (!existing.isPresent()) {
			throw new NoStudentExistsException(String.format("No student exists with id=%d", id));
		}
		return existing.get();
	
	}

	public List<Student> findByFirstnameStartingWith(String firstname) {
		LOGGER.debug("Retrieving the list of all students with firstname start with {}", firstname);
		return (List<Student>) studentRepository.findByFirstNameStartingWithIgnoreCase(firstname.toUpperCase(),
				Sort.by(Sort.Direction.ASC, "firstName"));
	}
	
	
	
	public Page<Student> findByAgePage(Integer age , Pageable page) {
		LOGGER.debug("Retrieving the list of all students by Age  with {}", age);
		
		
		Page <Student> sudentPage = studentRepository.findByAge(age, page) ; 
		 
		 
		 
		 
		 return sudentPage ;
	}

	@Transactional
	public Student update(@NotNull final Student student) {
		LOGGER.debug("Updating {}", student);
		Optional<Student> existing = studentRepository.findById(student.getStudentId());
		if (!existing.isPresent()) {
			throw new NoStudentExistsException(String.format("No student exists with id=%s", student.getStudentId()));
		}
		return studentRepository.save(student);
	}

	@Transactional
	public Student delete(@NotNull final long studentId) {
		LOGGER.debug("Deleting {}", studentId);
		Optional<Student> existing = studentRepository.findById(studentId);
		if (!existing.isPresent()) {
			throw new NoStudentExistsException(String.format("No student exists with id=%s", studentId));
		}
		studentRepository.deleteById(studentId);
		return existing.get();
	}
}
