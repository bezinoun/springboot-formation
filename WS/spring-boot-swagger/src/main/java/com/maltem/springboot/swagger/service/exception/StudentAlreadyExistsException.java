package com.maltem.springboot.swagger.service.exception;

public class StudentAlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public StudentAlreadyExistsException(final String message) {
		super(message);
	}
}
