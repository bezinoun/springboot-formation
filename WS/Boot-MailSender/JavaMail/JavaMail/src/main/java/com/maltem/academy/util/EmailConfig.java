package com.maltem.academy.util;

import java.io.File;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

@Configuration
public class EmailConfig {

	@Bean
	public JavaMailSender getJavaMailSender() {

		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost("smtp.gmail.com");
		mailSender.setPort(587);
		mailSender.setUsername("casaacademyjee@gmail.com");
		mailSender.setPassword("Azerty1234$");

		Properties properties = mailSender.getJavaMailProperties();
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.debug", "true");

		return mailSender;

	}
	
//	@Bean
//	public void sendMail() {
//		
//		SimpleMailMessage message = new SimpleMailMessage();
//		message.setTo("eladibdhiaeddine@gmail.com");
//		message.setFrom("casaacademyjee@gmail.com");
//		message.setSubject("Trace");
//		message.setText("FATAL ERROR - APPLICATION CRASH");
//		getJavaMailSender().send(message);
//		
//	}

	@Bean
	public void sendMailWithAttachement() {
		
		MimeMessage message = getJavaMailSender().createMimeMessage();
		MimeMessageHelper helper ;
		try {
			helper = new MimeMessageHelper(message,true);
			helper.setTo("eladibdhiaeddine@gmail.com");
			helper.setFrom("casaacademyjee@gmail.com");
			helper.setSubject("Trace");
			helper.setText("FATAL ERROR - APPLICATION CRASH");
			FileSystemResource file = 
			new FileSystemResource(new File("C:\\log\\log4j-application.log"));
			helper.addAttachment("log4j-application.log", file);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getJavaMailSender().send(message);
		
		
	}
}
