package com.maltem.academy.models;

public class Calculatrice {

	private double x;
	private double y;
	private String op;
	public Calculatrice() {
		super();
	}
	public Calculatrice(double x, double y, String op) {
		super();
		this.x = x;
		this.y = y;
		this.op = op;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	@Override
	public String toString() {
		return "Calculatrice [x=" + x + ", y=" + y + ", op=" + op + "]";
	}
	
	
	
	
	
}
