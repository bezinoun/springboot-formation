package com.maltem.academy.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maltem.academy.models.Calculatrice;

@Controller
public class CalculatriceController {
	
	@RequestMapping("/calculatrice")
	public String index() {
		
		return "calculatrice";
		
	}

	@RequestMapping("/calculer")
	public String calculer(@ModelAttribute Calculatrice calculatrice,Model model) {
		
		double resultat =0;
		
		if(calculatrice.getOp().equals("addition"))
			resultat = calculatrice.getX()+calculatrice.getY();
		else if(calculatrice.getOp().equals("soustraction"))
			resultat = calculatrice.getX()-calculatrice.getY();
		else if(calculatrice.getOp().equals("division"))
			resultat = calculatrice.getX()/calculatrice.getY();
		else if(calculatrice.getOp().equals("multiplication"))
			resultat = calculatrice.getX()*calculatrice.getY();
		
		model.addAttribute("result",resultat);
		
		
		
		
		
		
		
		
		return "calculatrice";
		
	}
	
	
}
