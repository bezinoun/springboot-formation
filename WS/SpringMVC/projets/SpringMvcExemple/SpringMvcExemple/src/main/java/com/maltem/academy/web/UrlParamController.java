package com.maltem.academy.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UrlParamController {

	@RequestMapping("/index")
	public String index() {
		
		return "urlParam";
		
	}
	@RequestMapping("/recupParam")
	public String recupererParam(@RequestParam String code) {
		System.out.println(code);
		return "urlParam";
		
		
	}
	
}
