package com.maltem.academy.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FirstController {
    
//	@RequestMapping("/accueil")
//	public String firstMeth() {
//		
//		return "index";
//		
//		
//	}
	
	@RequestMapping("/sentVar")
	public String sentVar(Model model) {
		String nom = "maltem";
		model.addAttribute("n",nom);
		return "index";
	}
	
	@RequestMapping("/recup")
	public String recupVal(String prenom) {
		
		System.out.println(prenom);
		return "index";
		
	}
	
}
