package com.maltem.academy;

import com.maltem.academy.imetier.IGenericMetier;
import com.maltem.academy.metier.PersonneMetier;
import com.maltem.academy.models.Personne;

public class Application {

	public static void main(String[] args) {
		
		IGenericMetier<Personne> metier = new PersonneMetier();
        Personne p = new Personne(1,"Haubert", "Thomas", 18);
        Personne p1 = new Personne(2,"Nasri", "Samir", 34);
        Personne p2 = new Personne(3,"Zakaria", "Zakaria", 28);
//        metier.update(p);
//        metier.ajouter(p);
//        metier.ajouter(p1);
//        metier.ajouter(p2);
//        metier.delete(p);
        System.out.println(metier.findAll());
	}

}
