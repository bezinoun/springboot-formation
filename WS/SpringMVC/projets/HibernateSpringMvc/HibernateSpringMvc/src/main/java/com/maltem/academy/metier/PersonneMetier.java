package com.maltem.academy.metier;

import java.util.List;

import com.maltem.academy.dao.PersonneDao;
import com.maltem.academy.idao.IGenericDao;
import com.maltem.academy.imetier.IGenericMetier;
import com.maltem.academy.models.Personne;

public class PersonneMetier implements IGenericMetier<Personne> {

	private IGenericDao<Personne> dao ;
	
	
	@Override
	public void ajouter(Personne Personne) {
		dao.ajouter(Personne);
		
	}

	@Override
	public void update(Personne Personne) {
		dao.update(Personne);
		
	}

	@Override
	public void delete(Personne Personne) {
		dao.delete(Personne);
		
	}

	@Override
	public Personne getById(int id) {
		
		return dao.getById(id);
	}

	@Override
	public List<Personne> findAll() {
	
		return dao.findAll();
	}

	public IGenericDao<Personne> getDao() {
		return dao;
	}

	public void setDao(IGenericDao<Personne> dao) {
		this.dao = dao;
	}

	
	
	
	
}
