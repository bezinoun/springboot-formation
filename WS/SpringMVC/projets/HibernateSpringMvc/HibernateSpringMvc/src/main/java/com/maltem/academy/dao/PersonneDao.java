package com.maltem.academy.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.maltem.academy.models.Personne;
import com.maltem.academy.util.HibernateUtil;

public class PersonneDao extends GenericDao<Personne> {

	@Override
	public Personne getById(int id) {
		SessionFactory sf = HibernateUtil.getConnection();
		Session s = sf.openSession();
		Personne p = s.get(Personne.class, id);
		s.close();
		return p;
	}

	@Override
	public List<Personne> findAll() {
		SessionFactory sf = HibernateUtil.getConnection();
		Session s = sf.openSession();

		Query query = s.createQuery("from Personne");
		List<Personne> personnes = query.getResultList();
		s.close();
		return personnes;
	}

}
