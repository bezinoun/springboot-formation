package com.maltem.academy.imetier;

import java.util.List;

public interface IGenericMetier<G> {
	public void ajouter(G g);
	public void update(G g);
	public void delete(G g);
	public G getById(int id);
	public List<G> findAll();
}
