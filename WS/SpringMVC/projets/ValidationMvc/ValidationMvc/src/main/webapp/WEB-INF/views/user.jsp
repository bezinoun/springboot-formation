<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Custom Validation</title>
<style>
.error{

color:red;
}

</style>
</head>
<body>
<form:form action="welcome.do" method="post" modelAttribute="utilisateur">
<table>
<tr>
<td>Email</td>
<td><form:input path="email"/></td>

</tr>
<tr>
<td>Password</td>
<td><form:input path="password"/></td>
<td><form:errors path="password" cssClass="error"/></td>
</tr>
<tr>

<td><form:button>submit</form:button></td>
</tr>


</table>


</form:form>
</body>
</html>