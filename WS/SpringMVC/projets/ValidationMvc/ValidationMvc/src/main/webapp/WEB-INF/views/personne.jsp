<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form"  prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Formulaire d'ajout</title>
<style>

.error{
color:red;
}

</style>
</head>
<body>
<form:form action="valider.do" method="post" modelAttribute="personne">
<table>
<tr>
<td>Nom</td>
<td><form:input path="nom"/></td>
<td><form:errors path="nom" cssClass="error" /></td>
</tr>
<tr>
<td>Prenom</td>
<td><form:input path="prenom"/></td>
<td><form:errors path="prenom" cssClass="error"/></td>
</tr>
<tr>
<td>Age</td>
<td><form:input path="age"/></td>
<td><form:errors path="age"  cssClass="error"/></td>
</tr>

<tr>
<td><form:button>submit</form:button></td>

</tr>
</table>


</form:form>
${msg}
</body>
</html>