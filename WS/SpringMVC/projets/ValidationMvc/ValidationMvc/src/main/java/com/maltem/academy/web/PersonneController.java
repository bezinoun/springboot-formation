package com.maltem.academy.web;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maltem.academy.models.Personne;

@Controller
public class PersonneController {
	
	@RequestMapping("/accueil")
	public String index (Model model) {
		model.addAttribute("personne",new Personne());
		
		return "personne";
	}
	
	@RequestMapping("/valider")
	public String valider(@Valid @ModelAttribute Personne personne,BindingResult br,Model model) {
		
		String message;
		if(br.hasErrors()) {
			
			return "personne";
		}
		else
		if(personne.getAge()>=18)
			message="Majeur";
		else
			message="Mineur";
		model.addAttribute("msg",message);
		
		
		return "resultat";
	}

}
