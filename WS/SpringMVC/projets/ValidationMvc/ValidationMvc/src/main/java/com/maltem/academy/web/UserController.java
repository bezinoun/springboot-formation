package com.maltem.academy.web;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.maltem.academy.models.Utilisateur;

@Controller
public class UserController {
    
	@RequestMapping("/login")
	public String index(Model model) {
		
		model.addAttribute("utilisateur",new Utilisateur());
		return "user";
	}
	@RequestMapping("/welcome")
	public String valider(@Valid @ModelAttribute Utilisateur utilisateur,BindingResult br,Model model) {
		
		
		String message="";
		if(br.hasErrors()) {
			
			return "user";
		}
		else if(utilisateur.getEmail().equals("test@test.com") && utilisateur.getPassword().equals("maltemjee2019"))
			message ="acc�s autoris�";
		else 
			message ="email or password incorrect";
		
		model.addAttribute("msg",message);
		
		return "accueil";
	}
	
}
