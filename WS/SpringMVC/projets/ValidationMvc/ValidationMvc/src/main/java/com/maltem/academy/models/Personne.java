package com.maltem.academy.models;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class Personne {

	
	
	@Size(min=1,message="Ce champ est obligatoire")
	private String nom;
	@Size(min=1,message="Ce champ est obligatoire")

	private String prenom;
	@Min(value=10,message="l'�ge doit �tre >=10")
	private int age;
	public Personne() {
		super();
	}
	public Personne( String nom, String prenom, int age) {
		super();
		
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
	}
   
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Personne [ nom=" + nom + ", prenom=" + prenom + ", age=" + age + "]";
	}
	
	
	
}
