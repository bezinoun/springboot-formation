package com.maltem.academy.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.maltem.academy.validation.PasswordValidator;

@Constraint(validatedBy=PasswordValidator.class)
@Target({ElementType.METHOD,ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Password {
	
	String message () default "Password doit contenir une chaine jee";
	Class<?>[] groups() default { };

	Class<? extends Payload>[] payload() default { };

}
