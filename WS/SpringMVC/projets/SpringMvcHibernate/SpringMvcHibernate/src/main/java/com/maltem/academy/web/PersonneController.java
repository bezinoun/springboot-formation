package com.maltem.academy.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.maltem.academy.imetier.IGenericMetier;
import com.maltem.academy.models.Personne;

@Controller
public class PersonneController {
	@Autowired
	private IGenericMetier<Personne> metier;
	private String mode = "ajout";
	private String label ="save";
	@RequestMapping("/accueil")
	public String index(Model model) {
		model.addAttribute("mode",mode);
		model.addAttribute("label",label);
		List<Personne> personnes = metier.findAll();
		model.addAttribute("personnes",personnes);
		
		return "personne";
		
	}
	
	@RequestMapping("/save")
	public String ajouter(@ModelAttribute Personne p, Model model) {
		if(mode.equals("ajout"))
		metier.ajouter(p);
		else if(mode.equals("update"))
			{
			metier.update(p);
			mode="ajout";
			label="save";
			}
		model.addAttribute("label",label);

		model.addAttribute("mode",mode);
		model.addAttribute("personne",new Personne());
		model.addAttribute("personnes",metier.findAll());
		
		
		
		return "personne";
	}
	
	@RequestMapping("/delete")
	public String delete(@RequestParam int id,Model model)
	{
		metier.delete(metier.getById(id));
		model.addAttribute("personnes",metier.findAll());
		model.addAttribute("label",label);

		model.addAttribute("mode",mode);
		return "personne";
		
		
	}
	
	@RequestMapping("/edit")
	public String edit(@RequestParam int id,Model model) {
		this.mode="update";
		this.label="update";
		model.addAttribute("label",label);
		model.addAttribute("mode",mode);
		model.addAttribute("personne",metier.getById(id));
		model.addAttribute("personnes",metier.findAll());
		
		
		return "personne";
		
	}
	
	
	
	
	
	
	

	public IGenericMetier<Personne> getMetier() {
		return metier;
	}

	public void setMetier(IGenericMetier<Personne> metier) {
		this.metier = metier;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	

}
