<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste de personnes</title>
</head>
<body>
	
		<table>
			<tr>
				<th>Id</th>
				<th>Nom</th>
				<th>Prenom</th>
				<th>Age</th>
			</tr>
			<c:forEach items="${personnes}" var="p">
				<tr>
					<td>${p.id}</td>
					<td>${p.nom}</td>
					<td>${p.prenom}</td>
					<td>${p.age}</td>
					
					
					<td><a href="edit.do?id=${p.id}">Edit</a></td>
				
					<td><a href="delete.do?id=${p.id}">Delete</a></td>

				</tr>
			</c:forEach>
		</table>


<form action="save.do" method="post">
<table>
<tr>
<td>Id</td>
<td><c:if test="${mode == 'ajout'}"><input type="text" name="id" value="${personne.id}"/></c:if>

<c:if test="${mode == 'update'}">${personne.id}<input type="hidden" name="id" value="${personne.id}"/></c:if>
</td>
</tr>
<tr>
<td>Nom</td>
<td><input type="text" name="nom" value="${personne.nom}"/></td>
</tr>
<tr>
<td>Prenom</td>
<td><input type="text" name="prenom" value="${personne.prenom}"/></td>
</tr>
<tr>
<td>Age</td>
<td><input type="text" name="age" value="${personne.age}"/></td>
</tr>
<tr>

<td><input type="submit" value="${label}"/></td>
</tr>

</table>

	</form>
</body>
</html>