package com.maltem.academy.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MajeurMineurController {

	@RequestMapping("/index")
	public String index() {
		
		return "majeurMineur";
		
	}
	@RequestMapping("/valider")
	public String valider(int age,Model model) {
		
		String msg;
		
		if(age >=18)
			msg="majeur";
		else
			msg="mineur";
		
		model.addAttribute("msg",msg);
		
		
		
		
		return "majeurMineur";
		
	}
	
	
}
