package com.maltem.academy.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	// comment il va chercher les utilisateurs et les roles
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//		auth.inMemoryAuthentication()
//		.withUser("admin").password("{noop}1234").roles("ADMIN","USER")
//		.and()
//		.withUser("user").password("{noop}1234").roles("USER");

//		auth.jdbcAuthentication()
//		.dataSource(dataSource)
//		.usersByUsernameQuery("select username as principal ,password as credentials,"
//				+ "true from user where username=?")
//		.authoritiesByUsernameQuery("select username as principal,role as role from user where username = ?")
//		.passwordEncoder(NoOpPasswordEncoder.getInstance())
//		.rolePrefix("ROLE_");

		auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
	}

	// les droits , c'est ici de décider d'ajouter des filtres ou pas
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();
		// utiliser le formulaire fournit par spring security
//		1
//		http.formLogin();
//		pour désactiver les sessions et le rendre stateless
//		2
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		// page login personnalisé
//	 option à la place de 1
//		http.formLogin().loginPage("/login");
		http.authorizeRequests().antMatchers("/login/**", "/register/**").permitAll();
		http.authorizeRequests().antMatchers(HttpMethod.POST, "/tasks/**").hasAuthority("ADMIN");
		http.authorizeRequests().anyRequest().authenticated();
		http.addFilter(new JWTAuthenticationFilter(authenticationManager()));
//		UsernamePasswordAuthenticationFilter càd c'est filter basé sur username et mdp
		http.addFilterBefore(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
	}

}
