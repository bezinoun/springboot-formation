package com.maltem.academy.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maltem.academy.entities.AppRole;

public interface RoleRepository extends JpaRepository<AppRole, Long> {

	public AppRole findByRoleName(String roleName);
	
}
