package com.maltem.academy.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maltem.academy.entities.AppUser;

public interface UserRepository extends JpaRepository<AppUser, Long> {
	
	public AppUser findByUsername(String username);

}
