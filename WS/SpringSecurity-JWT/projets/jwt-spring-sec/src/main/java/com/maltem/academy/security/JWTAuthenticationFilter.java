package com.maltem.academy.security;

import java.io.IOException;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.maltem.academy.entities.AppUser;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTAuthenticationFilter  extends UsernamePasswordAuthenticationFilter{
	private AuthenticationManager authenticationManger;
	
	
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManger) {
		super();
		this.authenticationManger = authenticationManger;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		AppUser appUser = null;
		
//		on le fait seulement lorsque les données sont envoyés en format www.url-encoded
//		String username=request.getParameter("username");
//		String password=request.getParameter("password");
		try {
//			ObjectMapper pour désérialiser l'objet json et le stocker dans AppUser
			appUser = new ObjectMapper().readValue(request.getInputStream(), AppUser.class);
		} catch (Exception e) {
//			pour que l'utilisateur reçoit une chose
			throw new RuntimeException(e);
		} 
		System.out.println("*******************************");
		System.out.println("username: "+appUser.getUsername());
		System.out.println("password: "+appUser.getPassword());
		return authenticationManger.authenticate(new UsernamePasswordAuthenticationToken(appUser.getUsername(), appUser.getPassword()));
	}
	
//	filterchain c'est les filtres utilisés par springsecurity,authresult resultat retourné par springsecurity
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		
//		pour retourner user avec getPrincipal()
		User springUser = (User) authResult.getPrincipal();
		
		String jwt = Jwts.builder()
				.setSubject(springUser.getUsername())
				.setExpiration(new Date(System.currentTimeMillis()+SecurityConstants.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS256, SecurityConstants.SECRET)
				.claim("roles", springUser.getAuthorities())
				.compact();
		response.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX+jwt);
		
	}

}
