package com.maltem.academy.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maltem.academy.entities.Task;


public interface TaskRepository extends JpaRepository<Task, Long> {

}
