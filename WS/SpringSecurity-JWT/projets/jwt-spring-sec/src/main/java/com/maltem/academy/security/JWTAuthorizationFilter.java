package com.maltem.academy.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JWTAuthorizationFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

//		j'autorise tout les domaines
		response.addHeader("Access-Control-Allow-Origin", "*");
//		quand à angular d'envoyer une requete http, il faut lui donner les entetes autorisés
//		je t'autorise à m'envoyer ses entetes (allow-headers)
		response.addHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,Authorization");
//		je t'autorise d'exposer ses entetes à tes applications(expose-headers)
		response.addHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Authorization");
//		OPTIONS permet au client http d'interoger le serveur pour fournir les options de communications 
//		à utiliser pour une resources tel que (méthodes autorisées , entetes autorisées,origine autorisés)
//		avant la requete post il envoie une requete options
		String jwt = request.getHeader(SecurityConstants.HEADER_STRING);
		System.out.println(jwt);
		if(request.getMethod().equals("OPTIONS")) {
			
			response.setStatus(HttpServletResponse.SC_OK);
		}
		
		else {
		if (jwt == null || !jwt.startsWith(SecurityConstants.TOKEN_PREFIX)) {

//			dofilter passe au filter suivant
			filterChain.doFilter(request, response);
			return;

		}
		Claims claims = Jwts.parser()
//				signe avec le secret
				.setSigningKey(SecurityConstants.SECRET)
//				supprimer le prefix
				.parseClaimsJws(jwt.replace(SecurityConstants.TOKEN_PREFIX, "")).getBody();
		String username = claims.getSubject();
		ArrayList<Map<String, String>> roles = (ArrayList<Map<String, String>>) claims.get("roles");
//		pour springsecurity il a besoin des roles sous format grantedauthority
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		roles.forEach(r -> {
//			chercher avec la clé authority
			authorities.add(new SimpleGrantedAuthority(r.get("authority")));

		});
		UsernamePasswordAuthenticationToken authenticatedUser = new UsernamePasswordAuthenticationToken(username,
				null, authorities);
//		charger l'utilisateur connecté dans le context de springsecurity
		SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
		filterChain.doFilter(request, response);
	}
	}
}
