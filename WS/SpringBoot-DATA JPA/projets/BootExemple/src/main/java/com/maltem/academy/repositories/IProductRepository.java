package com.maltem.academy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maltem.academy.models.Product;

public interface IProductRepository extends JpaRepository<Product, Long> {

}
