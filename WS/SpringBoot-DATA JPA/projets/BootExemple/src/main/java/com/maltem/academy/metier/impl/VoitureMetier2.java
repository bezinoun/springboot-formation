package com.maltem.academy.metier.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maltem.academy.metier.IVoitureMetier;
import com.maltem.academy.models.Voiture;
import com.maltem.academy.repositories.IVoitureRepository;

//@Service("vt2")

@Service
public class VoitureMetier2 implements IVoitureMetier{
	
	@Autowired
	private IVoitureRepository repo;

	@Override
	public void saveOrUpdate(Voiture v) {
		repo.save(v);
		
	}

	@Override
	public void delete(long id) {
		repo.deleteById(id);
		
	}

	@Override
	public Voiture getById(long id) {
		
		return repo.getOne(id);
	}

	@Override
	public List<Voiture> findAll() {
		
		return repo.findAll();
	}

	public IVoitureRepository getRepo() {
		return repo;
	}

	public void setRepo(IVoitureRepository repo) {
		this.repo = repo;
	}

	@Override
	public List<Voiture> findByPuissance(int puissance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Voiture> findByCouleurAndPuissance(String couleur, int puissance) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Voiture> chercherParModele(String modele) {
		// TODO Auto-generated method stub
		return null;
	}

}
