package com.maltem.academy.metier;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.maltem.academy.models.Voiture;

public interface IVoitureMetier {

	public void saveOrUpdate(Voiture v);

	public void delete(long id);

	public Voiture getById(long id);

	public List<Voiture> findAll();

	public List<Voiture> findByPuissance(int puissance);

	public List<Voiture> findByCouleurAndPuissance(String couleur, int puissance);
	
	public List<Voiture> chercherParModele(String modele);

}
