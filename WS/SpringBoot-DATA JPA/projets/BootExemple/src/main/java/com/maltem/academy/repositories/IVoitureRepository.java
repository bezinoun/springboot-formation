package com.maltem.academy.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maltem.academy.models.Voiture;


public interface IVoitureRepository extends JpaRepository<Voiture, Long>{

	
	List<Voiture> findByPuissance(int puissance);
	
    List<Voiture> findByCouleurAndPuissance(String couleur,int puissance);
    
//    @Query(value="select v from Voiture v where v.modele=:x")
//    List<Voiture> chercherParModele(@Param("x") String modele);
    
    @Query(value="select * from voiture where modele=:x",nativeQuery=true)
    List<Voiture> chercherParModele(@Param("x") String modele);
	
	
}
