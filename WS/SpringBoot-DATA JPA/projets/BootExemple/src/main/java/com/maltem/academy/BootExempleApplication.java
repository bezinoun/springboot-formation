package com.maltem.academy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maltem.academy.metier.IProductMetier;
import com.maltem.academy.metier.IVoitureMetier;
import com.maltem.academy.models.Product;
import com.maltem.academy.models.Voiture;

@SpringBootApplication
public class BootExempleApplication implements CommandLineRunner {
	@Autowired
//	@Qualifier("vt2")
	@Qualifier("voitureMetier")
	private IVoitureMetier metier;
	
	
	@Autowired
	private IProductMetier pmetier;

	public static void main(String[] args) {

		SpringApplication.run(BootExempleApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

//		Voiture v = new Voiture(1, "Mercedes", "CLA", "Noir", 9);
//		Voiture v1 = new Voiture(2, "Renault", "Clio", "Noir", 4);
//		Voiture v2 = new Voiture(3, "Peugeot", "206", "rouge", 4);
//		Voiture v3 = new Voiture(4, "Bmw", "E46", "Noir", 7);
//		metier.saveOrUpdate(v);
//		metier.saveOrUpdate(v1);
//		metier.saveOrUpdate(v2);
//		metier.saveOrUpdate(v3);
//		System.out.println(metier.getById(1));
//		System.out.println(metier.findAll());
//		metier.delete(1);
//		System.out.println(metier.findByPuissance(4));
//		System.out.println(metier.findByCouleurAndPuissance("Noir", 7));
//	    System.out.println(metier.chercherParModele("E46"));
		
		Product p = new Product(0, "Telephone", "Samsung S10 Plus", "Japan", 2000);
		Product p1 = new Product(2, "Voitures", "BMW X6", "Allemagne", 350000);
		Product p2 = new Product(0, "Frigidaire", "Lg", "Mexique", 3000);
		
//		pmetier.saveOrUpdate(p);
		pmetier.saveOrUpdate(p1);
//		pmetier.saveOrUpdate(p2);
		
		System.out.println(pmetier.findAll());
//		System.out.println(pmetier.get(1));
//		pmetier.delete(4);
	}

}
