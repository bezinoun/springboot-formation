package com.maltem.academy.metier;

import java.util.List;

import com.maltem.academy.models.Product;

public interface IProductMetier {

	public void saveOrUpdate(Product p);

	public Product get(long id);

	public List<Product> findAll();

	public void delete(long id);

}
