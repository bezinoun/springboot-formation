package com.maltem.academy.models;

public class Societe {

	private Long id;
	private String nomSociete;

	public Societe() {
		super();
	}

	public Societe(String nomSociete) {
		super();
		this.nomSociete = nomSociete;
	}

	public Societe(Long id, String nomSociete) {
		super();
		this.id = id;
		this.nomSociete = nomSociete;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomSociete() {
		return nomSociete;
	}

	public void setNomSociete(String nomSociete) {
		this.nomSociete = nomSociete;
	}

	@Override
	public String toString() {
		return "Societe [id=" + id + ", nomSociete=" + nomSociete + "]";
	}

}
