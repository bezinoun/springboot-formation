package com.maltem.academy.web;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.maltem.academy.models.Societe;





@RestController
public class SocieteGateWayWebService {
	

	private RestTemplate restTemplate;
	 
	@Autowired
	public SocieteGateWayWebService(RestTemplateBuilder builder) {
	    this.restTemplate = builder.build();
	}
	

	@RequestMapping(value="/names")
	//SIZE PAR DEFAUT 20
	public Collection<Societe>listSocietes(@RequestParam(defaultValue="0")int page,@RequestParam(defaultValue="30")int size)
	{  
		ParameterizedTypeReference<Resources<Societe>>responseType = new ParameterizedTypeReference<Resources<Societe>>() {
		};
		
		return restTemplate.exchange("http://societe-service/societes?page="+page+"&size="+size
				,HttpMethod.GET,null,responseType).getBody().getContent();
		
	}

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}


	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}



}
