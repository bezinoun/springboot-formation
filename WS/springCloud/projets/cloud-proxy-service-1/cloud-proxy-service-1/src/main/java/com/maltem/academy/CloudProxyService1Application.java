package com.maltem.academy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
@EnableEurekaClient
@EnableZuulProxy
@SpringBootApplication

public class CloudProxyService1Application {

	public static void main(String[] args) {
		SpringApplication.run(CloudProxyService1Application.class, args);
	}

}
