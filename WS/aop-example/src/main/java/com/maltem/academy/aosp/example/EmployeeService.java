package com.maltem.academy.aosp.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EmployeeService {
    @Autowired
    private Employee employee;

    public Employee getEmployee() {
        return employee;
    }

    @Loggable
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

	public EmployeeService(Employee employee) {
		super();
		this.employee = employee;
	}

	public EmployeeService() {
		super();
	}
    
    
}
