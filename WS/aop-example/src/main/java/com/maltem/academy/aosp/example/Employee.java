package com.maltem.academy.aosp.example;





@Loggable
public class Employee {
    private String name;

    public void throwException(){
        throw new RuntimeException("Dummy Exception");
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Employee(String name) {
		super();
		this.name = name;
	}

	public Employee() {
		super();
	}
    
	
	public String getFirstName() {
		return "Hello from FirstName Methode";
	}
    
}
