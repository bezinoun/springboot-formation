package com.maltem.academy.aosp.example.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class EmployeeAnnotationAspect {
    @Before("@annotation(com.maltem.academy.aosp.example.Loggable)")
    public void myAdvice(){
        System.out.println("Executing myAdvice!! with annotation");
    }

    @AfterReturning("execution(* com.maltem.academy.aosp.example.EmployeeService.setEmployee(..)) && @annotation(com.maltem.academy.aosp.example.Loggable)")
    public void logServiceAccess(JoinPoint joinPoint) {
        System.out.println("Completed + annotation: " + joinPoint);
    }
}
