package com.maltem.academy.aosp.example.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class EmployeeAspect {
    @Before("execution(public String getName())")
    public void getNameAdvice(){
        System.out.println("Executing Advice on getName()");
    }

    @Before("execution(* com.maltem.academy.aosp.example.EmployeeService.*.get*())")
    public void getAllAdvice(){
        System.out.println("Service method getter called");
    }
}
