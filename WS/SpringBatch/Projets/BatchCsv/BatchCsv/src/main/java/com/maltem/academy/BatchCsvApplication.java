package com.maltem.academy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.maltem.academy.entities.User;
import com.maltem.academy.repositories.IUserRepository;

@SpringBootApplication
public class BatchCsvApplication implements CommandLineRunner{
    @Autowired
	private IUserRepository repo;
	
	public static void main(String[] args) {
		SpringApplication.run(BatchCsvApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		User u = new User(1, "user1", "user1");
		User u1 = new User(2, "user2", "user2");
		User u2 = new User(3, "user3", "user3");
		User u3 = new User(4, "user4", "user4");
//		repo.save(u);
		repo.save(u1);
		repo.save(u2);
		repo.save(u3);
		
	}

}
