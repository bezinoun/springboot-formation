package com.maltem.academy.batch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.maltem.academy.entities.User;

public class UserRowMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setNom(rs.getString("nom"));
		user.setPrenom(rs.getString("prenom"));
		System.out.println(user);

		return user;
	}

}
