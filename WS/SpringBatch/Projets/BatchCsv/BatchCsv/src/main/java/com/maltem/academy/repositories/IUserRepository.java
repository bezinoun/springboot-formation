package com.maltem.academy.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maltem.academy.entities.User;

public interface IUserRepository extends JpaRepository<User, Integer>{

}
