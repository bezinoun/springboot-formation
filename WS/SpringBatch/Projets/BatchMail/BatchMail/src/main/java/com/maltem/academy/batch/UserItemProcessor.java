package com.maltem.academy.batch;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.maltem.academy.models.User;

public class UserItemProcessor implements ItemProcessor<User, MimeMessage> {

	@Autowired
	private JavaMailSender mailSender;
	
	private String sender;
	private String attachment;
	
	
	public VelocityEngine velocityEngine() throws VelocityException, IOException {
		
		VelocityEngineFactoryBean factory = new VelocityEngineFactoryBean();
		Properties properties = new Properties();
		properties.put("resource.loader","class");
		properties.put("class.resource.loader.class", 
				  "org.apache.velocity.runtime.resource.loader." + 
				  "ClasspathResourceLoader");	
		factory.setVelocityProperties(properties);
		return factory.createVelocityEngine();
		
	
	}
	
	
	
	
	public UserItemProcessor(String sender, String attachment) {
		super();
		this.sender = sender;
		this.attachment = attachment;
	}


	
	

	@Override
	public MimeMessage process(User user) throws Exception {
       MimeMessage message = mailSender.createMimeMessage();
       MimeMessageHelper helper = new MimeMessageHelper(message,true);
       Map<String,Object>model = new HashMap<>();
       model.put("name",user.getFullname());
       model.put("code",user.getCode());
       helper.setFrom(sender);
       helper.setTo(user.getEmail());
       helper.setCc(sender);
       helper.setSubject(VelocityEngineUtils
    		   .mergeTemplateIntoString(velocityEngine(), velocityEngine().getTemplate("templates/email-subject.txt").getName(), "UTF-8", model));
       helper.setText(VelocityEngineUtils
    		   .mergeTemplateIntoString(velocityEngine(), velocityEngine().getTemplate("templates/email-text.txt").getName(),"UTF-8",model));
		System.out.println("Message pour : "+user.getEmail());
		FileSystemResource file = new FileSystemResource(new File(attachment));
		helper.addAttachment(file.getFilename(), file);
		return message;
	}



	public JavaMailSender getMailSender() {
		return mailSender;
	}



	public String getSender() {
		return sender;
	}



	public void setSender(String sender) {
		this.sender = sender;
	}



	public String getAttachment() {
		return attachment;
	}



	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}



	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

}
