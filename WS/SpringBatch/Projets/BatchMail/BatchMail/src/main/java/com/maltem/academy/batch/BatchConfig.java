package com.maltem.academy.batch;

import javax.mail.internet.MimeMessage;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

import com.maltem.academy.models.User;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Value("${spring.mail.username}")
	private String sender;

	@Value("${batch.attachment}")
	private String attachment;

	@Value("${batch.data}")
	private String file;
	
	@Value("${batch.notifications.mail}")
	private String notificationEmail;

	@Bean
	public FlatFileItemReader<User> reader() {

		FlatFileItemReader<User> reader = new FlatFileItemReader<User>();
		reader.setResource(new FileSystemResource(file));
		reader.setLinesToSkip(1);
		reader.setLineMapper(new DefaultLineMapper<User>() {
			{

				setLineTokenizer(new DelimitedLineTokenizer() {
					{

						setNames(new String[] { "fullname", "code", "email" });

					}
				});
				
				setFieldSetMapper(new BeanWrapperFieldSetMapper<User>() {{
					
					setTargetType(User.class);
					
				}});

			

			}
		});
		
     return reader;
	}
	
	@Bean
	public UserItemProcessor processor() {
		
		return new UserItemProcessor(sender, attachment);
		
	}
	
	@Bean
	public UserItemWriter writer() {
		
		return new UserItemWriter();
		
	}
	
	@Bean
	public UserNotificationListener listener() {
		
		return new UserNotificationListener(notificationEmail);
		
	}
	
	@Bean
	public Step step1() {
		
		return stepBuilderFactory.get("step1").<User,MimeMessage>chunk(10)
				.reader(reader())
				.processor(processor())
				.writer(writer())
				.build();
		}
	
	@Bean
	public Job mailUserJob() {
		
		return jobBuilderFactory.get("mailUserJob")
				.incrementer(new RunIdIncrementer())
				.listener(listener())
				.flow(step1())
				.end()
				.build();
		
	}

}
