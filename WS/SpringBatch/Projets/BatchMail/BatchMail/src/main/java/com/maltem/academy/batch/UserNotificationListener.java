package com.maltem.academy.batch;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class UserNotificationListener extends JobExecutionListenerSupport {

	@Autowired
	private JavaMailSender mailSender;
	
	
	private String notificationEmail;
	
	
	
	
	
	
	
	
	
	
	

	public UserNotificationListener(String notificationEmail) {
		super();
		this.notificationEmail = notificationEmail;
	}
	
	@Override
	public void afterJob(JobExecution jobExecution) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setSubject("Job Completed");
		message.setFrom(notificationEmail);
		message.setTo(notificationEmail);
		message.setText("Job completed with "+jobExecution.getExitStatus());
		mailSender.send(message);
		
	}
	
	
	
	@Override
	public void beforeJob(JobExecution jobExecution) {
	SimpleMailMessage message = new SimpleMailMessage();
	message.setSubject("Job started");
	message.setFrom(notificationEmail);
	message.setTo(notificationEmail);
	message.setText("Job started");
	mailSender.send(message);
	
	}
	
	
	
	

	public JavaMailSender getMailSender() {
		return mailSender;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public String getNotificationEmail() {
		return notificationEmail;
	}

	public void setNotificationEmail(String notificationEmail) {
		this.notificationEmail = notificationEmail;
	}
	
	
	
	
}
