package com.maltem.academy.models;

public class User {
	
	private String fullname;
	private String code;
	private String email;
	public User() {
		super();
	}
	public User(String fullname, String code, String email) {
		super();
		this.fullname = fullname;
		this.code = code;
		this.email = email;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "User [fullname=" + fullname + ", code=" + code + ", email=" + email + "]";
	}
	
	

}
